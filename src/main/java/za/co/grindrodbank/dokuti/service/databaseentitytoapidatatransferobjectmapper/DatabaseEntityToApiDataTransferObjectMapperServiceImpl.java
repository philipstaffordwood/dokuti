/**
 * *************************************************
 * Copyright (c) 2020, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package za.co.grindrodbank.dokuti.service.databaseentitytoapidatatransferobjectmapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.openapitools.model.Attribute;
import org.openapitools.model.Document;
import org.openapitools.model.DocumentAttribute;
import org.openapitools.model.DocumentHeader;
import org.openapitools.model.DocumentVersion;
import org.openapitools.model.LookupTag;
import org.openapitools.model.SharedObject;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import za.co.grindrodbank.dokuti.attribute.AttributeEntity;
import za.co.grindrodbank.dokuti.document.DocumentEntity;
import za.co.grindrodbank.dokuti.documentattribute.DocumentAttributeEntity;
import za.co.grindrodbank.security.service.accesstokenpermissions.SecurityContextUtility;

@Service
public class DatabaseEntityToApiDataTransferObjectMapperServiceImpl implements DatabaseEntityToApiDataTransferObjectMapperService {

    @Override
    public Page<DocumentHeader> mapDocumentEntityPageToDocumentHeaderPage(Page<DocumentEntity> entities) {
        return entities.map(entity -> mapDocumentEntityToDocumentHeader(entity));
    }

    public DocumentHeader mapDocumentEntityToDocumentHeader(DocumentEntity entity) {
        DocumentHeader documentHeader = new DocumentHeader();
        // Map the base properties.
        BeanUtils.copyProperties(entity, documentHeader);
        // Map all document versions.
        List<DocumentVersion> documentVersions = mapDocumentVersions(entity);
        documentHeader.setDocumentVersions(documentVersions);

        // Map permissions for current user
        UUID userId = UUID.fromString(SecurityContextUtility.getUserIdFromJwt());
        entity.getDocumentPermissions().stream()//
                .filter(docPermissions -> !docPermissions.isTeam()) //
                .filter(docPermissions -> docPermissions.getUserId().equals(userId)) //
                .forEach(p -> documentHeader.addMyaccessItem(p.getPermission()));

        return documentHeader;
    }

    public Document mapDocumentEntityToDocument(DocumentEntity entity) {
        Document document = new Document();
        // Map the base properties.
        BeanUtils.copyProperties(entity, document);
        // Map all document versions.
        List<DocumentVersion> documentVersions = mapDocumentVersions(entity);
        document.setDocumentVersions(documentVersions);
        // Map all associated Tags.
        List<LookupTag> documentTags = new ArrayList<>();
        entity.getDocumentTags().forEach(documentTagEntity -> {
            LookupTag tag = new LookupTag();
            tag.setTag(documentTagEntity.getId().getTag());
            documentTags.add(tag);
        });
        document.setTags(documentTags);

        // Map all associated Document Attributes.
        List<DocumentAttribute> documentAttributes = new ArrayList<>();
        if (entity.getLatestDocumentVersion() != null) {
            entity.getLatestDocumentVersion().getDocumentAttributes().forEach(documentAttributeEntity -> {
                DocumentAttribute documentAttribute = mapDocumentAttributeEntityToDocumentAttribute(documentAttributeEntity);

                documentAttributes.add(documentAttribute);
            });
        }
        document.setAttributes(documentAttributes);

        // Map all associated Shared Objects (except for the current users permissions)
        UUID currentUserId = UUID.fromString(SecurityContextUtility.getUserIdFromJwt());
        HashMap<UUID, SharedObject> uSharedObjectMap = new HashMap<>();
        entity.getDocumentPermissions().forEach(docPermission -> {
            boolean isTeam = docPermission.isTeam();
            UUID sharedUuid = isTeam ? docPermission.getTeamId() : docPermission.getUserId();

            boolean isCurrentUsersUuid = currentUserId.equals(sharedUuid);
            if (!isCurrentUsersUuid) {
                if (uSharedObjectMap.containsKey(sharedUuid)) {
                    SharedObject dp = uSharedObjectMap.get(sharedUuid);
                    dp.addPermissionsItem(docPermission.getPermission());
                } else {
                    SharedObject p = new SharedObject();
                    p.addPermissionsItem(docPermission.getPermission());
                    p.setTeamflag(isTeam);
                    p.setUuid(sharedUuid.toString());

                    uSharedObjectMap.put(sharedUuid, p);
                }
            }
        });

        uSharedObjectMap.keySet().forEach(key -> document.addSharesItem(uSharedObjectMap.get(key)));

        return document;
    }

    /**
     * @param entity
     * @return
     */
    private List<DocumentVersion> mapDocumentVersions(DocumentEntity entity) {
        List<DocumentVersion> documentVersions = new ArrayList<>();
        entity.getDocumentVersions().forEach(documentVersionEntity -> {
            DocumentVersion documentVersion = new DocumentVersion();
            BeanUtils.copyProperties(documentVersionEntity, documentVersion);
            documentVersion.setDocumentVersionId(documentVersionEntity.getId());
            documentVersion.setCreatedDateTime(documentVersionEntity.getCreatedDateTime().toOffsetDateTime());
            documentVersions.add(documentVersion);
        });
        return documentVersions;
    }

    public Page<Document> mapDocumentEntityPageToDocumentPage(Page<DocumentEntity> entities) {
        return entities.map(entity -> mapDocumentEntityToDocument(entity));
    }

    public DocumentAttribute mapDocumentAttributeEntityToDocumentAttribute(DocumentAttributeEntity documentAttributeEntity) {
        DocumentAttribute documentAttribute = new DocumentAttribute();
        documentAttribute.setName(documentAttributeEntity.getAttribute().getName());
        documentAttribute.setValidationRegex(documentAttributeEntity.getAttribute().getValidationRegex());
        documentAttribute.setValue(documentAttributeEntity.getValue());

        return documentAttribute;
    }

    public LookupTag mapDocumentTagEntityToLookupTag(String tag) {
        LookupTag lookupTag = new LookupTag();
        lookupTag.setTag(tag);

        return lookupTag;
    }

    public Page<LookupTag> mapDocumentTagsPageToLookupTagPage(Page<String> tags) {
        return tags.map(tag -> mapDocumentTagEntityToLookupTag(tag));
    }

    public Attribute mapAttributeEntityToAttribute(AttributeEntity entity) {
        Attribute dto = new Attribute();
        BeanUtils.copyProperties(entity, dto);
        dto.setId(entity.getId().intValue());

        return dto;
    }

    public Page<Attribute> mapAttributeEntityPageToAttributePage(Page<AttributeEntity> entities) {
        return entities.map(entity -> mapAttributeEntityToAttribute(entity));
    }

}
