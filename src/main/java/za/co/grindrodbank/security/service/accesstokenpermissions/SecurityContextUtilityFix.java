/**
 * *************************************************
 * Copyright (c) 2020, Grindrod Bank Limited
 * License MIT: https://opensource.org/licenses/MIT
 * **************************************************
 */
package za.co.grindrodbank.security.service.accesstokenpermissions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class SecurityContextUtilityFix {

    private SecurityContextUtilityFix() {
    }	
	
    // TODO this need to moved to <groupId>za.co.grindrodbank</groupId> , <artifactId>security</artifactId>
    @SuppressWarnings("unchecked")
    public static List<String> getTeamsFromJwt() {
        Map<String, Object> claims = SecurityContextUtility.getClaimsFromJwt();
        if (claims != null) {
            try {
                return (List<String>) claims.get("team");
            } catch (ClassCastException e) {
                List<String> teams = new ArrayList<>();

                try {
                    String team = (String) claims.get("team");

                    if (team == null) {
                        return new ArrayList<>();
                    }
                    teams.add(team);

                    return teams;
                } catch (Exception a) {
                    return new ArrayList<>();
                }
            }

        }

        return new ArrayList<>();    	
    	
    }
}
